//
//  ADNOSSync.h
//  Time Tracker
//
//  Created by Keitaroh Kobayashi on 3/31/14.
//  Copyright (c) 2014 kkob. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ADNKit.h>

extern NSString * ADNOSSyncOperationFinished;

@interface ADNOSSync : NSObject

@property (nonatomic, strong) ANKClient *syncClient;
@property (nonatomic, strong, readonly) ANKChannel *syncChannel;
@property (nonatomic, strong) NSString *syncChannelIdentifier;

/** An array of strings represented the classes that are synced. */
@property (nonatomic, strong) NSArray *syncedModels;

+ (instancetype) sharedSync;

- (void) triggerSync;

@end
