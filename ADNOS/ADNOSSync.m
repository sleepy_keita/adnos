//
//  ADNOSSync.m
//  Time Tracker
//
//  Created by Keitaroh Kobayashi on 3/31/14.
//  Copyright (c) 2014 kkob. All rights reserved.
//

#import "ADNOSSync.h"

#import "ADNOSBackedModel.h"

#import <ADNKit.h>

NSString * ADNOSSyncOperationFinished = @"ADNOSSyncOperationFinished";

@interface ADNOSSync ()

- (void) syncWithChannel:(ANKChannel *) channel;
- (void) enqueueMessage:(ANKMessage *) message instance:(ADNOSBackedModel*) instance;

- (void) restartSyncUpOperations:(NSTimer *) timer;
- (void) pauseSyncOperationsFor:(NSInteger) seconds;

@end

@interface AFHTTPClient ()

@property (readwrite, nonatomic, strong) NSOperationQueue *operationQueue;

@end

@implementation ADNOSSync {
    ANKClient * __strong _syncClient;

    NSOperationQueue * __strong _syncUpOperations;
    NSTimer * __strong _restartSyncUpOperationQueueTimer;

    NSDate * __strong _syncStartedDate;
}

- (instancetype) init {
    if ((self = [super init])) {
        NSString *bundleIdentifier = [[NSBundle mainBundle] bundleIdentifier];
        _syncChannelIdentifier = [bundleIdentifier stringByAppendingString:@".sync-b"];

        _syncUpOperations = [[NSOperationQueue alloc] init];
        _syncUpOperations.maxConcurrentOperationCount = 1;
        _syncUpOperations.name = @"ADNOSSync Queue";
    }
    return self;
}

- (void) triggerSync {
    _syncStartedDate = [NSDate date];
    NSLog(@"Starting sync at %@.", _syncStartedDate.description);

    if (_syncUpOperations.operationCount > 0) {
        NSLog(@"Previous sync session hasn't finished. Aborting...");
        return;
    }

    if (!_syncChannel) {
        // First, we'll try to find the channel from ADN.
        [self.syncClient fetchCurrentUserSubscribedChannelsWithTypes:@[
                                                                       self.syncChannelIdentifier
                                                                       ]
                                                              completion:
         ^(id responseObject, ANKAPIResponseMeta *meta, NSError *error) {
             NSArray *channels = responseObject;

             if (channels.count >= 1) {
                 _syncChannel = channels[0];
                 [self syncWithChannel:channels[0]];
             } else {
                 ANKACL *onlyMeACL = [ANKACL ACLForUsers:@[self.syncClient.authenticatedUser]];
                 onlyMeACL.isPublic = NO;
                 onlyMeACL.canAnyUser = NO;

                 [self.syncClient createChannelWithType:self.syncChannelIdentifier
                                                    readers:nil
                                                    writers:nil
                                                 completion:
                  ^(id responseObject, ANKAPIResponseMeta *meta, NSError *error) {
                      if (error != nil) {
                          NSLog(@"Error in creating the sync channel: %@", error);
                      } else {
                          _syncChannel = responseObject;
                          [self syncWithChannel:responseObject];
                      }
                  }];
             }
         }];
        
    } else {
        [self syncWithChannel:self.syncChannel];
    }
}

- (void) syncWithChannel:(ANKChannel *) channel {
    // First, pull changes.
    NSString *lastSyncedId = [self.syncClient.userDefaults stringForKey:@"lastSyncedMessageId-A"];
    ANKClient *currentClient = nil;

    if (lastSyncedId) {
        currentClient =
        [self.syncClient clientWithPagination:[ANKPaginationSettings settingsWithSinceID:lastSyncedId
                                                                                beforeID:@""
                                                                                   count:200]];
    } else {
        currentClient =
        [self.syncClient clientWithPagination:[ANKPaginationSettings settingsWithCount:200]];
    }

    __block BOOL isMore = YES;

    dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
    dispatch_queue_t queue = dispatch_queue_create("kkSyncQueue", DISPATCH_QUEUE_CONCURRENT);

    dispatch_async(queue, ^{
        NSArray * __block changesToApply = @[];

        while (isMore) {
            [currentClient fetchMessagesInChannel:_syncChannel
                                       completion:
             ^(id responseObject, ANKAPIResponseMeta *meta, NSError *error) {
                 NSArray *messages = responseObject;

                 changesToApply = [changesToApply arrayByAddingObjectsFromArray:messages];

                 currentClient.pagination.beforeID = meta.minID;
                 isMore = meta.moreDataAvailable;

                 dispatch_semaphore_signal(semaphore);
             }];
            
            dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
        }

        NSEnumerator *reverseEnumerator = changesToApply.reverseObjectEnumerator;

        NSUInteger maxId = 0;

        for (ANKMessage *message in reverseEnumerator) {
            NSArray *annotations = [message annotationsWithType:@"us.kkob.syncmsg"];
            NSDictionary *messageData = ((ANKAnnotation *)annotations[0]).value;

            NSString *className = messageData[@"c"];
            // This model isn't recognized by this app.
            if (![self.syncedModels containsObject:className]) continue;

            Class objClass = NSClassFromString(className);

            NSDictionary *syncDictionary = messageData[@"d"];

            ADNOSBackedModel *instance = (ADNOSBackedModel*)[objClass instanceWithPrimaryKey:syncDictionary[@"id"] createIfNonexistent:NO];
            if (!instance) {
                instance = (ADNOSBackedModel*)[objClass firstInstanceWhere:@"\"adnMessageId\" = ?", message.messageID];

                if (!instance) {
                    instance = (ADNOSBackedModel*)[objClass instanceWithPrimaryKey:syncDictionary[@"id"]];
                }
            }

            maxId = MAX(maxId, message.messageID.integerValue);

            if (instance.synced == YES && instance.adnMessageId) {
                // This instance has already been synced,
                // so we'll skip over it and continue with the next one.
                continue;
            }

            [instance loadDictionaryFromSync:syncDictionary];
            instance.adnMessageId = @(message.messageID.integerValue);

            [instance save];
        }

        if (maxId > 0)
            [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%ld", (long)maxId] forKey:@"lastSyncedMessageId-A"];

        // Now, push the changes we've made over here.
        for (NSString *className in self.syncedModels) {
            Class objClass = NSClassFromString(className);
            NSArray *instancesToSync = [objClass instancesWhere:@"\"synced\" = '0' AND \"availableToSync\" = '1'"];
            for (ADNOSBackedModel *instance in instancesToSync) {
                if (!instance.existsInDatabase) continue;
                
                ANKMessage *theMessage = [ANKMessage new];

                theMessage.isMachineOnly = YES;

                NSDictionary *value = @{
                                        @"c": className,
                                        @"d": instance.dictionaryForSync
                                        };

                ANKAnnotation *dataAnnotation = [ANKAnnotation annotationWithType:@"us.kkob.syncmsg"
                                                                            value:value];


                theMessage.annotations = @[dataAnnotation];

                [self enqueueMessage:theMessage instance:instance];
            }

            instancesToSync = nil;
        }

        [_syncUpOperations waitUntilAllOperationsAreFinished];

        // Now, delete all spam entries.

        for (NSString *className in self.syncedModels) {
            Class objClass = NSClassFromString(className);
            [objClass executeUpdateQuery:@"DELETE FROM \"$T\" WHERE \"synced\" = '1' AND \"adnMessageId\" IS NULL"];
        }

        if (_syncStartedDate) {
            NSLog(@"Finished sync (%ld s).", (long)fabs([_syncStartedDate timeIntervalSinceNow]));
            _syncStartedDate = nil;
        }

        [[NSNotificationCenter defaultCenter] postNotificationName:ADNOSSyncOperationFinished object:nil];
    });

}

- (void) enqueueMessage:(ANKMessage *) message
               instance:(ADNOSBackedModel *)instance {

    [self.syncClient createMessage:message
                         inChannel:self.syncChannel
                        completion:
     ^(id responseObject, ANKAPIResponseMeta *meta, NSError *error) {
         if (error != nil) {
             if (meta.statusCode == 429) {
                 // Rate limited.
                 [self pauseSyncOperationsFor:meta.rateLimitRetryAfter.integerValue];

                 // Re-enqueue to be run after the operation queue is re-started.
                 [self enqueueMessage:message instance:instance];
             } else {
                 NSLog(@"Error in pushing changes: %@", error);
             }
         } else {
             ANKMessage *savedMessage = responseObject;

             instance.synced = YES;
             instance.adnMessageId = @( savedMessage.messageID.integerValue );
             [instance save];

             if (meta.rateLimitRemaining.integerValue <= 2) {
                 [self pauseSyncOperationsFor:meta.rateLimitReset.integerValue];
             }
         }
     }];
}

- (void) restartSyncUpOperations:(NSTimer *) timer {
    [_syncUpOperations setSuspended:NO];
}

- (void) pauseSyncOperationsFor:(NSInteger) seconds {
    [_syncUpOperations setSuspended:YES];

    if (_restartSyncUpOperationQueueTimer)
        [_restartSyncUpOperationQueueTimer invalidate];

    _restartSyncUpOperationQueueTimer =
    [NSTimer scheduledTimerWithTimeInterval:seconds
                                     target:self
                                   selector:@selector(restartSyncUpOperations:)
                                   userInfo:nil
                                    repeats:NO];

}

#pragma mark - Properties.

- (ANKClient *) syncClient {
    if (_syncClient) {
        return _syncClient;
    } else {
        NSAssert(1==0, @"You must set syncClient.");
    }

    return nil;
}

- (void) setSyncClient:(ANKClient *)syncClient {
    if (!_syncClient || [_syncClient isEqualTo:syncClient]) {
        [self willChangeValueForKey:@"syncClient"];

        _syncClient = [syncClient copy];

        _syncClient.generalParameters.includeAnnotations = YES;
        _syncClient.operationQueue = _syncUpOperations;

        [self didChangeValueForKey:@"syncClient"];
    }
}

#pragma mark - Singleton

+ (instancetype) sharedSync {
    static dispatch_once_t pred;
    static ADNOSSync *shared = nil;

    dispatch_once(&pred, ^{
        shared = [[ADNOSSync alloc] init];
    });

    return shared;
}

@end
