//
//  ADNOSBackedModel.h
//  Time Tracker
//
//  Created by Keitaroh Kobayashi on 3/31/14.
//  Copyright (c) 2014 kkob. All rights reserved.
//

#import <FCModel.h>
#import <ADNKit.h>

@interface ADNOSBackedModel : FCModel

/** Whether this model has been synced up to ADN yet */
@property BOOL synced;

/** Whether this model is eligible to be synced up to ADN */
@property BOOL availableToSync;

/** The App.net message ID this entry corresponds to */
@property NSNumber *adnMessageId;

/** A NSDictionary of keys and values prepared to go to ADN */
- (NSDictionary *) dictionaryForSync;

/** Loads the NSDictionary received from ADN into the receiver. Note that you must send the `save` message to the receiver to persist changes. */
- (void) loadDictionaryFromSync:(NSDictionary *) syncDictionary;

@end
