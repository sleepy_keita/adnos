//
//  ADNOSBackedModel.m
//  Time Tracker
//
//  Created by Keitaroh Kobayashi on 3/31/14.
//  Copyright (c) 2014 kkob. All rights reserved.
//

#import "ADNOSBackedModel.h"

// Private FCModel methods we're going to need to use.
@interface FCModel ()

- (id)encodedValueForFieldName:(NSString *)fieldName;
- (void)decodeFieldValue:(id)value intoPropertyName:(NSString *)propertyName;

@end

@implementation ADNOSBackedModel

- (NSDictionary *) dictionaryForSync {
    NSArray *keys = [[self class] databaseFieldNames];
    NSMutableDictionary *dict = [@{} mutableCopy];
    for (NSString *key in keys) {
        if ([@[@"synced", @"availableToSync", @"adnMessageId"] containsObject:key]) continue;
        dict[key] = [self encodedValueForFieldName:key];
    }

    return [dict copy];
}

- (void) loadDictionaryFromSync:(NSDictionary *) syncDictionary {
    for (NSString *key in syncDictionary) {
        if ([@[@"synced", @"availableToSync", @"id", @"adnMessageId"] containsObject:key]) continue;
        [self decodeFieldValue:[syncDictionary objectForKey:key] intoPropertyName:key];
    }
    
    self.synced = YES;
    self.availableToSync = YES;
}

- (id)valueOfFieldName:(NSString *)fieldName byResolvingReloadConflictWithDatabaseValue:(id)valueInDatabase {
    if ([@[@"synced", @"availableToSync", @"adnMessageId"] containsObject:fieldName])
        return [self valueForKeyPath:fieldName];

    return [super valueOfFieldName:fieldName byResolvingReloadConflictWithDatabaseValue:valueInDatabase];
}

@end
