# ADNOS

App.net Object Sync for Objective-C.

Built on top of [FCModel](https://github.com/marcoarment/FCModel), [FMDB](https://github.com/ccgus/fmdb), and [ADNKit](https://github.com/joeldev/ADNKit).

This is a very alpha implementation. It's currently being used in [Toki](https://kkob.us/toki/) as a "production sandbox" to test some new ideas out.

For more information about the protocol and mechanism itself, please read [my blog post about ADNOS](https://kkob.us/2014/04/20/app-net-object-sync/).

